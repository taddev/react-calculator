import React, { Component } from 'react';
import './Calculator.css';

class Box extends Component {
    render() {
        const { type, value, onClick } = this.props;
        return <button className={type} onClick={() => onClick(type, value)}>{value}</button>;
    }
}

class Display extends Component {
    render() {
        return <div className="display-panel">{this.props.value}</div>;
    }
}

class Board extends Component {
    renderBox = (type, value) => {
        const { handleButtonClick } = this.props;
        return <Box type={type} value={value} onClick={handleButtonClick} />;
    }

    render() {
        return(
            <div className="grid-container">
                {this.renderBox("clear", "clear")}
                {this.renderBox("operator", <span>&divide;</span>)}

                {this.renderBox("number", 7)}
                {this.renderBox("number", 8)}
                {this.renderBox("number", 9)}
                {this.renderBox("operator", <span>&times;</span>)}

                {this.renderBox("number", 4)}
                {this.renderBox("number", 5)}
                {this.renderBox("number", 6)}
                {this.renderBox("operator", <span>&minus;</span>)}

                {this.renderBox("number", 1)}
                {this.renderBox("number", 2)}
                {this.renderBox("number", 3)}
                {this.renderBox("operator", "+")}

                {this.renderBox("zero", 0)}
                {this.renderBox("number", ".")}
                {this.renderBox("operator", "=")}
            </div>
        );
    }
}

class Calculator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: [0],
            count: 0,
            operands: Array(2).fill(0),
            operator: null,
            decimal: false,
            done: false,
            type: null
        };
    }

    generateDisplay = () => {
        const { display } = this.state;
        return display.join("");
    }

    handleClear = () => {
        this.setState({
            display: [0],
            count: 0,
            operands: Array(2).fill(0),
            operator: null,
            decimal: false,
            done: false,
            type: null
        });
    }

    doMath = () => {
        const { operands, operator } = this.state;
        let display = [0];

        if (operator === "+") {
            display[0] = operands[0] + operands[1];
        } else if (operator === "<span>&minus;</span>") {
            display[0] = operands[0] - operands[1];
        }

        this.setState({
            display: display,
            done: true
        });
    }

    handleButtonClick = (type, value) => {
        if (value === "clear") {
            this.handleClear();
        } else {
            const { done } = this.state;
            let { display, decimal, count, operands } = this.state;

            if (done) {
                count = 0;
                display = [0];
                this.handleClear();
            }

            // don't allow more than one decimal point
            if (value === "." && decimal) {
                return;
            }

            if (type === "operator") {
                if (value === "=") {
                    this.doMath();
                    return;
                } else if (count < 2) {
                    this.setState({
                        display: [0],
                        operator: value,
                        count: count + 1,
                        decimal: false,
                    });
                }
            }

            if (type === "number" || type === "zero") {
                // stop at nine digits
                if (display.length > 8) {
                    return;
                }
                
                // let the first digit replace the zero unless it's a decimal
                if (display.length === 1 && display[0] === 0 && value !== ".") {
                    display[0] = value;
                } else {
                    if (value === ".") {
                        decimal = true;
                    }

                    display.push(value);
                }

                operands[count] = parseFloat(display.join(""));
                this.setState({
                    display: display,
                    operands: operands,
                    type: type,
                    decimal: decimal
                })
            }
        }
    }

    render() {
        return (
            <div className="calculator">
                <Display value={this.generateDisplay()}/>
                <Board handleButtonClick={this.handleButtonClick}/>
            </div>
        );
    }
}

export default Calculator;
